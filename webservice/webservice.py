#!flask/bin/python3
from flask import Flask, request, jsonify
from lxml import etree
from flask_cors import CORS, cross_origin

from controllers.MinerController import MinerController

app = Flask(__name__)
CORS(app)

@app.route('/')
def main():
    return 'works'

@app.route('/storeData', methods=['POST'])
def store():
  parsedXml = etree.fromstring(request.data)
  minerController = MinerController()

  return minerController.store(parsedXml)

@app.route('/getData')
def get():
  minerController = MinerController()

  return jsonify(minerController.get())

if __name__ == '__main__':
    app.run(debug=True)
