from helpers.xmlHelper import extractInformationFromXml
from helpers.util import parseNumber

tags = {
  'UPLOAD': 'upload',
  'DOWNLOAD': 'download'
}

def getRamOrCPUAveraga(dataList):
  totalItems = len(dataList)
  totalSum = 0

  for item in dataList:
    totalSum += parseNumber(item.text)

  return round(totalSum / totalItems)

def getNetworkAverage(dataList):
  totalItems = len(dataList)
  uploadTotal = 0
  downloadTotal = 0

  if(totalItems == 0):
    return {
      tags['UPLOAD']: 0,
      tags['DOWNLOAD']: 0
    }

  for networkElement in dataList:
    upload = extractInformationFromXml(tags['UPLOAD'], networkElement)
    download = extractInformationFromXml(tags['DOWNLOAD'], networkElement)

    uploadTotal += float(upload.text)
    downloadTotal += float(download.text)

  return {
    tags['UPLOAD']: round(uploadTotal / totalItems),
    tags['DOWNLOAD']: round(downloadTotal / totalItems)
  }
