def extractInformationFromXml(tagName, xmlAsList):
  for xmlElement in xmlAsList:
    if(xmlElement.tag == tagName):
      return xmlElement
