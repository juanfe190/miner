from pymongo import MongoClient, DESCENDING

DATABASE = 'miner_monitor'
COLLECTION = 'miner_monitor'

class MinerMonitorModel:
  def __init__(self):
    self.connection = MongoClient()
    self.db = self.connection[DATABASE]
    self.collection = self.db[COLLECTION]

  def store(self, document):
    return self.collection.insert_one(document).inserted_id

  def getLast(self):
    return list(self.collection.find({}, { '_id': 0 }).sort('_id', DESCENDING).limit(1))
