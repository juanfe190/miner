from lxml import etree

from helpers.xmlHelper import extractInformationFromXml
from helpers.DataAnalyzer import getRamOrCPUAveraga, getNetworkAverage
from models.MinerMonitor import MinerMonitorModel

tags = {
  'RAM': 'ram',
  'CPU': 'cpu',
  'NETWORK': 'network'
}

class MinerController:
  def store(self, xmlAsList):
    networkData = extractInformationFromXml(tags['NETWORK'], xmlAsList)
    ramData = extractInformationFromXml(tags['RAM'], xmlAsList)
    cpuData = extractInformationFromXml(tags['CPU'], xmlAsList)

    networkAverage = getNetworkAverage(networkData)
    ramAverage = getRamOrCPUAveraga(ramData)
    cpuAverage = getRamOrCPUAveraga(cpuData)

    minerModel = MinerMonitorModel()

    minerModel.store({
      tags['RAM']: ramAverage,
      tags['CPU']: cpuAverage,
      tags['NETWORK']: networkAverage
    })

    return 'Record saved correctly on the database'

  def get(self):
    minerModel = MinerMonitorModel()
    data = minerModel.getLast()[0]

    return data
