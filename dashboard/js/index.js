(function() {
  var HOST = 'http://localhost:5000/getData'

  function startLiveUpdates() {
    fetchDataAndPrint()
    setInterval(fetchDataAndPrint, 5000)
  }

  function fetchDataAndPrint() {
    $.get(HOST, function(data) {
      document.getElementById('ram').innerHTML = data.ram
      document.getElementById('cpu').innerHTML = data.cpu
      document.getElementById('netDown').innerHTML = Math.floor(data.network.download / 1000)
      document.getElementById('netUp').innerHTML = Math.floor(data.network.upload / 1000)
      document.getElementById('lastUpdate').innerHTML = "Last update: " + new Date()
    })
  }

  startLiveUpdates()

})()
