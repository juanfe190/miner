import time
import psutil
import speedtest

def getNetworkSpeed():
  print('Starting network test...')
  servers = []
  s = speedtest.Speedtest()

  s.get_servers(servers)
  s.get_best_server()
  s.download()
  s.upload()
  print('Network results received')

  return s.results.dict()
