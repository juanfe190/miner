import threading
import time

from recollectors.RamCollector import RamCollector
from recollectors.CPUCollector import CPUCollector
from recollectors.NetworkCollector import NetworkCollector
from models.DataModel import DataModel
from handlers.DataHandler import DataHandler

model = DataModel()
ram = RamCollector(model)
cpu = CPUCollector(model)
network = NetworkCollector(model)
dataSender = DataHandler(model)

ram.start()
cpu.start()
network.start()
dataSender.start()
