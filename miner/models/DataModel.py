class DataModel:
  RAM = 'ram'
  CPU = 'cpu'
  NETWORK = 'network'

  data = {
    'ram': [],
    'cpu': [],
    'network': []
  }

  def push(self, key, value):
    self.data[key].append(value)

  def getAll(self):
    return self.data

  def clean(self):
    self.data = {
      'ram': [],
      'cpu': [],
      'network': []
    }
