import time
from threading import Thread

class Recollector(Thread):
  def __init__(self, model):
    Thread.__init__(self)

    self.isRunning = True
    self.model = model

  def stop(self):
    self.isRunning = False

  def run(self):
    while self.isRunning:
      self.collectData()
      time.sleep(self.interval)
