import psutil

from .recollector import Recollector
from helpers.networkHelper import getNetworkSpeed

class NetworkCollector(Recollector):
  interval = 25

  def collectData(self):
    model = self.model
    networkSpeed = getNetworkSpeed()
    networkStats = {
      'upload': networkSpeed['upload'],
      'download': networkSpeed['download']
    }

    model.push(model.NETWORK, networkStats)
