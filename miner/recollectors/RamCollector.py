import psutil

from .recollector import Recollector

class RamCollector(Recollector):
  interval = 20

  def collectData(self):
    model = self.model
    memory = psutil.virtual_memory()
    percentage = round(memory.available / memory.total * 100)

    model.push(model.RAM, percentage)
