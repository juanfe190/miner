import psutil

from .recollector import Recollector

class CPUCollector(Recollector):
  interval = 0.500

  def collectData(self):
    model = self.model

    model.push(model.CPU, psutil.cpu_percent())
