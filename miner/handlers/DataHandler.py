import time
import requests
from threading import Thread
from dicttoxml import dicttoxml

HOST_URL = 'http://localhost:5000/storeData'

class DataHandler(Thread):
  def __init__(self, model, interval = 60):
    Thread.__init__(self)

    self.isRunning = True
    self.interval = interval
    self.model = model

  def stop(self):
    self.isRunning = False

  def run(self):
    while self.isRunning:
      time.sleep(self.interval)
      self._startSendingData()

  def _startSendingData(self):
    xml = dicttoxml(self.model.getAll())
    headers = { 'Content-Type': 'application/xml' }

    print(requests.post(HOST_URL, data=xml, headers=headers).text)
